#ifndef _FaceDetectionENCLAVE_H_
#define _FaceDetectionENCLAVE_H_

#include <stdlib.h>
#include <assert.h>

#if defined(__cplusplus)
extern "C" {
#endif

	void printf(const char *fmt, ...);

#if defined(__cplusplus)
}
#endif

#endif /* !_FaceDetectionENCLAVE_H */