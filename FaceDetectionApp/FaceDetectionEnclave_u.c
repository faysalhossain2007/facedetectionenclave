#include "FaceDetectionEnclave_u.h"
#include <errno.h>

typedef struct ms_ocall_print_t {
	char* ms_str;
} ms_ocall_print_t;

static sgx_status_t SGX_CDECL FaceDetectionEnclave_ocall_print(void* pms)
{
	ms_ocall_print_t* ms = SGX_CAST(ms_ocall_print_t*, pms);
	ocall_print((const char*)ms->ms_str);

	return SGX_SUCCESS;
}

static const struct {
	size_t nr_ocall;
	void * func_addr[1];
} ocall_table_FaceDetectionEnclave = {
	1,
	{
		(void*)(uintptr_t)FaceDetectionEnclave_ocall_print,
	}
};

sgx_status_t generate_random_number(sgx_enclave_id_t eid)
{
	sgx_status_t status;
	status = sgx_ecall(eid, 0, &ocall_table_FaceDetectionEnclave, NULL);
	return status;
}

